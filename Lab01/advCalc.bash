#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-09-01 17:15:48 -0400 (Tue, 01 Sep 2015) $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Lab01/advCalc.bash $
#$Revision: 81284 $
#$Id: advCalc.bash 81284 2015-09-01 21:15:48Z ee364f05 $

 while read -r line || [[ -n $line ]]
    do
     return_code=$(./simpleCalc.bash $line)
     if(( $? != 0)) 
     then
	 echo "$line: Error"
     else
	 echo "$line: $return_code"
     fi
 done <$@

exit 0