#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-09-01 16:44:49 -0400 (Tue, 01 Sep 2015) $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Lab01/getStudentData.bash $
#$Revision: 81251 $
#$Id: getStudentData.bash 81251 2015-09-01 20:44:49Z ee364f05 $

total_students=0
grades=0
sum=0
average_grade=0
highest_score=0
student_name=0

if (($# != 1))
then
    echo "Usage: ./getStudentData.bash <course name>"
    exit 1
elif [[ $@ != "ece364" ]] && [[ $@ != "ece337" ]]
then
    echo "Error: course $@ is not a valid option."
    exit 2
else
    if [[ $@ = "ece337" ]]
    then
	total_students=$(cat ./gradebooks/ece337_*.txt |cut -d',' -f1 | wc -l)
	grades=$(cat ./gradebooks/ece337_*.txt | cut -d',' -f2)
	for item in ${grades[*]}
	do
	let sum=$sum+$item
	done
	let average_grade=$sum/$total_students
	highest_score=$(cat ./gradebooks/ece337_*.txt | sort -t ',' -k2 | tail -n1 | cut -d',' -f2)
	student_name=$(cat ./gradebooks/ece337_*.txt | sort -t ',' -k2 | tail -n1 | cut -d',' -f1)
	echo "Total Students: $total_students"	
	echo "Average score: $average_grade"
	echo "$student_name had the highest score of $highest_score"
    elif  [[ $@ = "ece364" ]]
    then
	total_students=$(cat ./gradebooks/ece364_*.txt |cut -d',' -f1 | wc -l)
	grades=$(cat ./gradebooks/ece364_*.txt | cut -d',' -f2)
	for item in ${grades[*]}
	do
	let sum=$sum+$item
	done
	let average_grade=$sum/$total_students
	highest_score=$(cat ./gradebooks/ece364_*.txt | sort -t ',' -k2 | tail -n1 | cut -d',' -f2)
	student_name=$(cat ./gradebooks/ece364_*.txt | sort -t ',' -k2 | tail -n1 | cut -d',' -f1)
	echo "Total Students: $total_students"	
	echo "Average score: $average_grade"
	echo "$student_name had the highest score of $highest_score"
    fi
fi

exit 0
