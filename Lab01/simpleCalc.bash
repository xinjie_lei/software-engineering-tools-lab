#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-09-01 16:44:49 -0400 (Tue, 01 Sep 2015) $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Lab01/simpleCalc.bash $
#$Revision: 81251 $
#$Id: simpleCalc.bash 81251 2015-09-01 20:44:49Z ee364f05 $

result=0

if (($# != 3))
then
    echo "Usage: ./simpleCalc.bash <operator> <operand1> <operand2>"
    exit 1
elif [[ $1 != "add" ]] && [[ $1 != "sub" ]] && [[ $1 != "mul" ]] && [[ $1 != "div" ]] && [[ $1 != "exp" ]] && [[ $1 != "mod" ]]
then
    echo "Error: invalid operator."
    exit 2
else
    if [[ $1 = "add" ]]
    then
	let result=$2+$3
	echo "$2 + $3 = $result"
    elif [[ $1 = "sub" ]]
    then
	let result=$2-$3
	echo "$2 - $3 = $result"
    elif [[ $1 = "mul" ]]
    then
	let result=$2*$3
	echo "$2 * $3 = $result"
    elif [[ $1 = "div" ]]
    then
	let result=$2/$3
	echo "$2 / $3 = $result"
    elif [[ $1 = "exp" ]]
    then
	let result=$2**$3
	echo "$2 ^ $3 = $result"
    elif [[ $1 = "mod" ]]
    then
	let result=$2%$3
	echo "$2 % $3 = $result"
    fi
fi

exit 0
