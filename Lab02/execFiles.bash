#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-09-08 17:51:57 -0400 (Tue, 08 Sep 2015) $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Lab02/execFiles.bash $
#$Revision: 81725 $
#$Id: execFiles.bash 81725 2015-09-08 21:51:57Z ee364f05 $

if (($# != 0))
then
    echo "Usage: ./execFiles.bash"
    exit 1
else
    ls c-files/* > source.txt
    while read -r line || [[ -n $line ]] 
    do
        if gcc -Wall -Werror $line >/dev/null 2>&1 
        then
            file=$(echo $line | cut -d'/' -f2) 
            echo "Compiling file $file... Compilation succeeded"
            filename=$(echo $file | cut -d'.' -f1) 
            ./a.out > $filename.out
        else
            file=$(echo $line | cut -d'/' -f2) 
            echo "Compiling file $file... Error: Compilation failed"
        fi
    done < source.txt
    
fi
exit 0
