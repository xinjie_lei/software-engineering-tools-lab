#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-09-08 17:23:06 -0400 (Tue, 08 Sep 2015) $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Lab02/printUsageStats.bash $
#$Revision: 81708 $
#$Id: printUsageStats.bash 81708 2015-09-08 21:23:06Z ee364f05 $

if (($# != 1))
then
    echo "Usage: ./printUsageStats.bash <filename>"
    exit 1
elif [[ ! -e $@ ]]
then
    echo "Error: $@ does not exist."
    exit 2
else
    timestamp=$(head -n 1 $@ | cut -d' ' -f3)
    echo "Parsing file \"$@\". Timestamp: $timestamp"
    echo "Your choices are:"
    echo "1) Active user IDs"
    echo "2) Highest CPU usage"
    echo "3) Top 3 longest running processes"
    echo "4) All processes by a specific user"
    echo "5) Exit"
    read -p "Please enter your choice: " choice
    if ((choice == 1)) 
    then
        total_users=$(head -n 1 $@ | cut -d' ' -f8)
        echo "Total number of active user IDs: $total_users"
    elif ((choice == 2))
    then
        highest_cpu_user=$(head -n 8 $@ | tail -n 1 | cut -d' ' -f2)
        highest_cpu_rate=$(head -n 8 $@ | tail -n 1 | cut -d' ' -f9)
        echo "User $highest_cpu_user is utilizing the highest CPU resources at %$highest_cpu_rate"
    elif ((choice == 3))
    then
        longest_time1=$(tail -n +8 $@ | sort  -k11 -n -r | head -n 1)
        longest_time2=$(tail -n +8 $@ | sort  -k11 -n -r | head -n 2 | tail -n 1)
        longest_time3=$(tail -n +8 $@ | sort  -k11 -n -r | head -n 3 | tail -n 1)
        echo $longest_time1
        echo $longest_time2
        echo $longest_time3
    elif ((choice == 4))
    then
        read -p "Please enter a valid username: " user
        read -p "Please enter a filename to save user's processes: " filename
        while read -r line || [[ -n $line ]]
        do
            data=$(echo $line | cut -d' ' -f2)
            if [[ $data = $user ]]
            then
                echo $line >> $filename
                check=1
            fi
        done < <(tail -n +8 $@)
        if (( check == 1 ))
        then
            echo "Output written to file $filename"
            echo " "
            read -p "Please enter your choice: " choice1
            exit 0
        else 
            echo "No match found"
            read -p "Please enter your choice: " choice1
            exit 0
        fi
    elif ((choice == 5))
    then
        exit 0
    fi

fi

exit 0
