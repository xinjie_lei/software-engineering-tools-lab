#! /usr/bin/env python3.4

#$Authors$
#$Date: 2015-09-15 17:44:48 -0400 (Tue, 15 Sep 2015) $
#$Revision: 81932 $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Lab03/myOperations.py $
#$Id: myOperations.py 81932 2015-09-15 21:44:48Z ee364f05 $


def checkTypes(l):
    numInt = 0
    numFloat = 0
    numStr = 0
    if type(l) is list:
        if len(l) == 0:
            return None
        else:
            for i in l:
                if type(i) is int:
                    numInt += 1
                elif type(i) is float:
                    numFloat += 1
                elif type(i) is str:
                    numStr += 1
    else:
        return None
    return [numInt, numFloat, numStr]


def normalizeVector(vector):
    sum = 0
    result = [] * len(vector)
    k = 0
    if type(vector) is list:
        if len(vector) == 0:
            return None
        else:
            for i in vector:
                sum += i
            for i in vector:
                result.insert(k, i/sum)
                k += 1
    else:
        return None
    return result


def findMedian(vList):
    if type(vList) is list:
        if len(vList) == 0:
            return None
        else:
            for i in vList:
                if type(i) is int or type(i) is float:
                    median = 0
                else:
                    return None
            vList.sort()
            ind = int(len(vList)/2) - 1
            if len(vList) % 2 == 0:
                median = (vList[ind] + vList[ind+1])/2
            else:
                median = vList[ind]
    else:
        return None
    return median


def rectifySignal(signal):
    result = []* len(signal)
    k = 1
    if type(signal) is list:
        if len(signal) == 0:
            return None
        else:
            for i in signal:
                if i < 0:
                    result.insert(k, 0)
                else:
                    result.insert(k, i)
            k += 1
    else:
        return None
    return result


def convertToBoolean(num):
    k = 0
    if type(num) is int:
        if 0 > num or num > 255:
            return None
        else:
            binary = bin(num)
            result = [] * 8
            for i in binary[2:]:
                if i == "0":
                    result.insert(k, False)
                else:
                    result.insert(k, True)
                k += 1
    else:
        return None
    return result


def convertToInteger(boolList):
    rList = []
    k = 0
    if type(boolList) is list:
        if len(boolList) == 0:
            return None
        else:
            for i in boolList:
                if type(i) is int:
                    return None
                elif i:
                    rList.insert(k, 1)
                elif not i:
                    rList.insert(k, 0)
                else:
                    return None
                k += 1
            result = 128 * rList[0] + 64 * rList[1] + 32 * rList[2] + 16 * rList[3] + 8 * rList[4] + 4 * rList[5] + 2 * rList[6] + rList[7]
    else:
        return None
    return result


def switchNames(nameList):
    result = []
    k = 1
    ffname = ""
    fullname = [] * 2
    fname = []
    name = []
    if type(nameList) is list:
        if len(nameList) == 0:
            return None
        else:
            for i in nameList:
                name = i.split(",")
                fname = name[1].split(" ")
                fullname.insert(0, fname[1])
                fullname.insert(1, name[0])
                ffname = " ".join(fullname)
                result.insert(k, ffname)
                name = []
                fname = []
                fullname = []
                k += 1

    else:
        return None
    return result


def getWeightAverage(data):
    result = 0
    sum = 0
    weight = []
    if type(data) is list:
        if len(data) == 0:
            return None
        else:
            for i in data:
                weight = i.split(":")
                lbs = weight[1].split(" ")
                sum += float(lbs[1])
            result = sum / len(data)
    else:
        return None
    return result