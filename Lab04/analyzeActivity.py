#! /usr/bin/env python3.4

from pprint import pprint as pp

def getUserPermissions():
    dict = {}
    with open('Permissions.txt', 'r') as file:
        list = file.readlines()
        list = list[2:]
        permission = set()
        for i in list:
            pair = i.replace(":", "").split()
            if pair[0] not in dict:
                permission = set()
                dict[pair[0]] = permission.add(pair[1])
            else:
                permission.add(pair[1])
                dict[pair[0]] = permission
    return dict


def getControllerPermissions():
    dict = {}
    user = {}
    others = []
    with open('Users.txt','r') as user_file:
        userlist = user_file.readlines()
        userlist = userlist[2:]
        for i in userlist:
            pair = i.replace("|", "").split()
            pair[0] = " ".join(pair[:2])
            user[pair[2]] = pair[0]
    with open('Permissions.txt', 'r') as file:
        permlist = file.readlines()
        permlist = permlist[2:]
        while len(permlist) > 0:
            group = set()
            others = []
            for i in permlist:
                perm = i.replace(":", "").split()
                if perm[1] not in dict:
                    if len(group) == 0:
                        dict[perm[1]] = group.add(user[perm[0]])
                    else:
                        others.append(i)
                else:
                    group.add(user[perm[0]])
                    dict[perm[1]] = group
            permlist = others
    return dict


def getControllerActions():
    dict = {}
    with open('ActivityLog.txt', 'r') as file:
        actlist = file.readlines()
        while len(actlist) > 1:
            group = set()
            others = []
            for i in actlist:
                perm = i.split('/')
                perm[4] = perm[4].replace("\n", "")
                if perm[3] not in dict:
                    if len(group) == 0:
                        group.add(perm[4])
                        dict[perm[3]] = group
                    else:
                        others.append(i)
                else:
                    group.add(perm[4])
                    dict[perm[3]] = group
            actlist = others
    return dict


def parseLogFile():
    log = []
    dict = getUserPermissions()

    with open('ActivityLog.txt', 'r') as file:
        actlist = file.readlines()
        for i in actlist:
            user = i.split(" : ")
            user[1] = user[1].replace('\n', '')
            web = user[1].split("/")
            web[1] = web[0] + '//' + web[2] + '/' + web[3] + '/' + web[4]
            if web[3] in dict[user[0]]:
                usert = (user[0], web[1], web[3], web[4], True)
            else:
                usert = (user[0], web[1], web[3], web[4], False)
            log.append(usert)
    return log


def canGrantAccess(userId, url):
    log = parseLogFile()
    for user, addr, _, _, trueness in log:
        if userId == user:
            if addr == url:
                return trueness


def checkUserActivity(userID):
    log = parseLogFile()
    actlist = []
    for user, addr, _, _, trueness in log:
        if user == userID:
            tupl = (addr, trueness)
            actlist.append(tupl)
    return actlist


def getActivityByUser():
    user = {}
    dict = {}
    group = ()
    with open('Users.txt','r') as user_file:
        userlist = user_file.readlines()
        userlist = userlist[2:]
        for i in userlist:
            pair = i.replace("|", "").split()
            pair[0] = " ".join(pair[:2])
            user[pair[2]] = pair[0]
    log = parseLogFile()
    while len(log) > 0:
        corr = 0
        fail = 0
        others = []
        currentuser, _, _, _, _ = log[0]
        for i in log:
            userid, _, _, _, trueness = i
            if userid == currentuser:
                if trueness == True:
                    corr += 1
                else:
                    fail += 1
            else:
                    others.append(i)
        dict[user[currentuser]] = (corr, fail)
        log = others
    return dict


def getActivityByController():
    dict = {}
    log = parseLogFile()
    while len(log) > 0:
        corr = 0
        fail = 0
        others = []
        _, _, act, _, _ = log[0]
        for i in log:
            _, _, acti, _, trueness = i
            if acti == act:
                if trueness == True:
                    corr += 1
                else:
                    fail += 1
            else:
                others.append(i)
        dict[act] = (corr, fail)
        log = others
    return dict