#! /bin/bash 

#$Authors$
#$Date: 2015-09-29 18:33:02 -0400 (Tue, 29 Sep 2015) $
#$Revision: 82345 $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Lab05/benchmarks.bash $
#$Id: benchmarks.bash 82345 2015-09-29 22:33:02Z ee364f05 $

result=()
echo -n "Enter the array size(s): " 
read -a size
echo -n "Enter the algorithm(s) to run: " 
read -a algo
read -p "Enter column # to sort benchmarks:" col

while (( $col > ${#size[*]} ))
do
    echo "Error: invalid column number."
    read -p "Enter column # to sort benchmarks: " col
done

echo -n "size," > $1
IFS=,
echo -n "${size[*]}" >> $1
echo "" >> $1

for i in ${algo[*]}
do
    n=0
    echo -n "$i," >> $1
    for j in ${size[*]}
    do
        ./sorting $j $i > /dev/null
        if (( $? == 2 ))
        then
           echo "Error: invalid size of array"
        fi
        ./sorting $j $i > /dev/null
        if (( $? == 5 )) 
        then
            echo "Error: invalid algorithm name"
        else
            result[$n]=$(./sorting $j $i | cut -d' ' -f3 )
        fi
        ((n=$n+1))
    done
    IFS=,
    echo "${result[*]}" >> $1
    result=()
done

echo -n "size," > $1.sorted
IFS=,
echo -n "${size[*]}" >> $1.sorted
echo "" >> $1.sorted


echo $(tail -n +2 $1 | sort -t',' -n -k$col) >> $1.sorted

exit 0
