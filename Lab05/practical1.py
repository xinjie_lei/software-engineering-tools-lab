#! /usr/bin/env python3.4

from pprint import pprint as pp


def verifySolution(fileName):
    tmp =[]
    n = 0
    with open(fileName, 'r') as file:
        line = file.readlines()
    for i in line:
        i = set(list(i.replace("\n", "")))
        if len(i) == 9:
            check1 = True
        else:
            check1 = False
            break
    while n <= 8:
        for i in line:
            tmp.append(list(i)[n])
        if len(set(tmp)) == 9:
            check2 = True
        else:
            check2 = False
            break
        tmp = []
        n += 1
        if not check2:
            break
    return check1 and check2


def solvePuzzle(sourceFileName, targetFilename):
    return


def getStudentInfo():
    student = {}
    with open('Students.txt','r') as file:
        stulist = file.readlines()
    stulist = stulist[2:]
    for i in stulist:
        pair = i.replace("|", "").split()
        pair[0] = " ".join(pair[:2])
        student[pair[2][1:]] = pair[0]

    return student


def getCallersOf(phoneNumber):
    caller = set()
    student = getStudentInfo()
    with open('Call Log.txt', 'r') as file:
        calllist = file.readlines()
    calllist = calllist[2:]
    for i in calllist:
        phone = i.split()
        phone[3] = " " + phone[3]
        key = "".join(phone[2]+phone[3])
        if phoneNumber == key:
            stu = phone[1].split("-")[1]
            caller.add(student[stu])
    result = sorted(list(caller))
    return result


def calcTime( timeTotal, timeIn ):
    if timeTotal[2] + timeIn[2] >= 60:
        sum_second = timeTotal[2] + timeIn[2] - 60
        minute = 1
    else:
        sum_second = timeTotal[2] + timeIn[2]
        minute = 0
    if timeTotal[1] + timeIn[1] >= 60:
        sum_minute= timeTotal[1] + timeIn[1] - 60
        hour = 1
    else:
        sum_minute= timeTotal[1] + timeIn[1]
        hour = 0
    timeTotal[2] = sum_second
    timeTotal[1] = sum_minute + minute
    timeTotal[0] = timeTotal[0] + timeIn[0] + hour
    return timeTotal


def getCallActivity():
    dict = {}
    student = getStudentInfo()
    with open("Call Log.txt", 'r') as file:
        line = file.readlines()
    line = line[2:]
    while len(line) > 0:
        m = 1
        group = []
        others = []
        group.append(line[0])
        key = line[0].split()[1].split("-")[1]
        duration = line[0].split()[4].split(":")
        duration.insert(0,'0')
        duration[2] = int(duration[2]); duration[1] = int(duration[1]); duration[0] = int(duration[0])
        dict[student[key]] = 1
        for i in line[1:]:
            stu = i.split()[1].split("-")[1]
            if stu == key:
                m += 1
                dict[student[key]] += 1
                timein = i.split()[4].split(":")
                timein.insert(0,'0')
                timein[2] = int(timein[2]); timein[1] = int(timein[1]); timein[0] = int(timein[0])
                duration = calcTime(duration, timein)
                group.append(i)
            else:
                others.append(i)
        duration[2] = str(duration[2]); duration[1] = str(duration[1]); duration[0] = str(duration[0]).zfill(2)
        time = ":".join(duration)
        pair = (m, time)
        dict[student[key]] = pair
        line = others
    return dict