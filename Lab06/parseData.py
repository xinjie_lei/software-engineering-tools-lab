#! usr/bin/env python3.4

import re


def getUserName(i):
    name = re.search(r"^[a-zA-Z]+,*\s*[a-zA-Z]+", i)
    if re.match(r"[a-zA-Z]+,", name.group()):
        namelist = re.split(r"\W+", name.group())
        tmp = namelist[0]
        namelist[0] = namelist[1]
        namelist[1] = tmp
        newname = " ".join(namelist)
        return newname
    else:
        return name.group()


def getInvalidUsers():
    list = []
    with open('UserData.txt', 'r') as file:
        line = file.readlines()
    for i in line:
        match = re.match(r"^[a-zA-Z]+,*\s*[a-zA-Z]+(,|\s)*$", i)
        if match is not None:
            name = getUserName(match.group())
            list.append(name)
    list.sort()
    return list


def getEmail(i):
    email = re.search(r"[a-zA-Z0-9|_|-|.]+@[a-zA-Z0-9|_|-|.]+", i)
    return email.group()


def getUsersWithEmails():
    newlist = []
    dict = {}
    with open('UserData.txt', 'r') as file:
        line = file.readlines()
    for i in line:
        match = re.match(r"^[a-zA-Z]+,*\s*[a-zA-Z]+(,|\s)*([a-zA-Z0-9|_|-|.])+@([a-zA-Z0-9|_|-|.])+(,|\s)*$", i)
        if match is not None:
            name = getUserName(i)
            email = getEmail(i)
            dict[name] = email
    li = list(dict.keys())
    li = sorted(li)
    for i in li:
        item = (i, dict[i])
        newlist.append(item)
    return newlist


def getPhone(i):
    phone = re.search(r"\(?[0-9]{3}\)?(\s|-)?[0-9]{3}-?[0-9]{4}", i)
    newphone = re.search(r"([0-9]{3})-([0-9]{3})-([0-9]{4})", phone.group())
    if newphone is not None:
        return "(" + newphone.group(1) + ") " + newphone.group(2) + "-" + newphone.group(3)
    else:
        newphone = re.search(r"(\([0-9]{3}\))\s([0-9]{3})-([0-9]{4})", phone.group())
        if newphone is not None:
            return newphone.group(1) + " " + newphone.group(2) + "-" + newphone.group(3)
        else:
            newphone = re.search(r"([0-9]{3})([0-9]{3})([0-9]{4})", phone.group())
            return "(" + newphone.group(1) + ") " + newphone.group(2) + "-" + newphone.group(3)

def getUsersWithPhones():
    newlist = []
    dict = {}
    with open('UserData.txt', 'r') as file:
        line = file.readlines()
    for i in line:
        match = re.search(r"^[a-zA-Z]+,*\s*[a-zA-Z]+(,|\s)*\(?[0-9]{3}\)?(\s|-)?[0-9]{3}-?[0-9]{4}(,|\s)*$", i)
        if match is not None:
            name = getUserName(i)
            newphone = getPhone(i)
            dict[name] = newphone
    li = list(dict.keys())
    li = sorted(li)
    for i in li:
        item = (i, dict[i])
        newlist.append(item)
    return newlist
