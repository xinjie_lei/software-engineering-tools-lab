#! usr/bin/env python3.4

class Course:
    def __init__(self, courseID, fst, snd, final):
        self.courseID = courseID
        self.fst = fst
        self.snd = snd
        self.final = final
        self.total = 0.25 * self.fst + 0.25 * self.snd + 0.50 * self.final
        self.letter = self.getLetterGrade()

    def __str__(self):
        retn_str = "{}: ({:0.2f}, {:0.2f}, {:0.2f}) = ({:0.2f}, {})".format(self.courseID, self.fst, self.snd, self.final, self.total, self.letter)
        return retn_str

    def getLetterGrade(self):
        if self.total >= 90:
            return 'A'
        elif 80 <= self.total < 90:
            return 'B'
        elif 70 <= self.total < 80:
            return 'C'
        elif 60 <= self.total < 70:
            return 'D'
        else:
            return 'F'


class Student:

    def __init__(self, name):
        self.name = name
        self.courses = {}

    def addCourse(self, course):
        self.courses[course.courseID] = course

    def __str__(self):
        retn_str = self.name + ": "
        course = list(self.courses.keys())
        course.sort()
        for i in course:
            retn_str += "({}: {}), ".format(i, self.courses[i].letter)
        return retn_str[:-2]

    def generateTranscript(self):
        retn_str = self.name + "\n"
        tmp = list(self.courses)
        tmp.sort()
        for i in tmp:
            retn_str += str(self.courses[i]) + "\n"
        return retn_str[:-1]


class School:

    def __init__(self, name):
        self.name = name
        self.students = {}

    def __str__(self):
        retn_str = "{}: {} Students\n".format(self.name, len(self.students))
        tmp = list(self.students)
        tmp.sort()
        for i in tmp:
            retn_str += self.students[i].name + "\n"
        return retn_str[:-1]

    def loadData(self, filename):
        with open(filename, 'r') as file:
            lines = file.read()
        line = lines.split("\n\n")
        for i in line:
            tmp = i.split("\n")
            student = Student(tmp[0])
            for j in tmp[2:]:
                courseID = j.split(":")[0].strip()
                fst = float(j.split(",")[0].split(":")[1].strip())
                snd = float(j.split(",")[1].strip())
                final = float(j.split(",")[2].strip())
                course = Course(courseID, fst, snd, final)
                student.addCourse(course)
            self.students[student.name] = student

    def saveData(self, filename):
        retn_li = []
        tmp = list(self.students)
        tmp.sort()
        for i in tmp:
            retn_li.append(self.students[i].generateTranscript())
        retn_str = "\n\n".join(retn_li)
        with open(filename, 'w') as file:
            file.write(retn_str)