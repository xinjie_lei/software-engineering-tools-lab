#! usr/bin/env python3.4

import vtools


def convertLine(verilogLine):
    try:
        netlist = vtools.parseNetLine(verilogLine)
        portmap = "("
        for i in netlist[2]:
            tmp = i[0] + "=>" + i[1] + ", "
            portmap += tmp
        portmap = portmap[:-2] + ");"
        return str(netlist[1]) + ": " + str(netlist[0]) + " PORT MAP" + portmap
    except ValueError:
        return "Error: Bad Line."


def convertFile(sourceFile, targetFile):
    result = ""
    with open(sourceFile, 'r') as sFile:
        lines = sFile.readlines()
    for i in lines:
        result += convertLine(i) + "\n"
    with open(targetFile, 'w') as tFile:
        tFile.write(result[:-1])