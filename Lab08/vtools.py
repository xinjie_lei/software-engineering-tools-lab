#! usr/bin/env python3.4

import re
import sys
import string


def isValidName(identifier):
    for i in identifier:
        if i in string.ascii_letters or i in string.digits or i == '_':
            pass
        else:
            return False
    return True


def parsePinAssignment(assignment):
    if assignment[0] != '.':
        raise ValueError(assignment)
    else:
        result = re.split(r"\((.*)", assignment)
        port = result[0].replace(".", "")
        if isValidName(port) is False:
            raise ValueError(assignment)
        pin = re.match(r"(_|\w)*\)$", result[1])
        if pin is None:
            raise ValueError(assignment)
        pin = pin.group().replace(")", "")
        if isValidName(pin) is False:
            raise ValueError(assignment)
        else:
            return port, pin


def parseNetLine(line):
    result = line.split()
    assignmentlist = []
    if len(result) < 3:
        raise ValueError(line)
    if isValidName(result[0]) is False:
        raise ValueError(line)
    if isValidName(result[1]) is False:
        raise ValueError(line)
    else:
        tmp = "".join(result[2:])
        ali = tmp.split(",")
        ali[0] = ali[0].replace("(", "", 1)
        ali[-1] = ali[-1].replace(")", "", 1)
        for i in ali:
            assignment = parsePinAssignment(i)
            assignmentlist.append(assignment)
    return result[0], result[1], tuple(assignmentlist)
