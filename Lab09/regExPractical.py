#! usr/bin/env python3.4
import re


def getNumber(stringInput):
    match = re.search(r"[+-]?\d\.\d*[Ee][+-]?\d*", stringInput)
    if match is None:
        return None
    else:
        return match.group()


def getOptions(commandline):
    result = []
    match = re.finditer(r"-(?P<opt>[a-z])\s+(?P<value>[^-\s]+)", commandline)
    for m in match:
        option = (m.group("opt"), m.group("value"))
        result.append(option)
    result.sort()
    return result


def getAddressParts(url):
    match = re.fullmatch(r"http(s)?://(?P<base>(\w|\.)+)/(?P<controller>[^._/]+)/(?P<action>[^._/]+)", url)
    if match is None:
        return None
    else:
        return match.group("base"), match.group("controller"), match.group("action")


def getAttributes(xmlSnippet):
    result = []
    match = re.finditer(r"(?P<tagname>[a-z]+)=\"(?P<tagvalue>.*?)\"", xmlSnippet)
    for m in match:
        tag = (m.group("tagname"), m.group("tagvalue"))
        result.append(tag)
    result.sort()
    return result

