#! usr/bin/env python3.4
class Duration:

    def __init__(self, hours=0, minutes=0, seconds=0):
        if type(hours) is not int or type(minutes) is not int or type(seconds) is not int:
            raise TypeError(Duration)
        elif hours < 0 or minutes < 0 or seconds < 0:
            raise ValueError(Duration)
        else:
            secd = int(seconds % 60)
            minu = int((minutes + (seconds - secd) / 60) % 60)
            hour = int(hours + (minutes - (minutes % 60)) / 60)
            self.hours = hour
            self.minutes = minu
            self.seconds = secd
            self._totalSeconds = self.getTotalSeconds()

    def __str__(self):
        retn_str = "{:0>2d}:{:0>2d}:{:0>2d}".format(self.hours, self.minutes, self.seconds)
        return retn_str

    def getTotalSeconds(self):
        return self.hours * 3600 + self.minutes * 60 + self.seconds

    def __add__(self, other):
        if isinstance(other, Duration) is False:
            raise TypeError(lambda x, y: x + y)
        else:
            result = Duration()
            result.seconds = int((self.seconds + other.seconds) % 60)
            result.minutes = int(((self.minutes + other.minutes) + (self.seconds + other.seconds - result.seconds) / 60) % 60)
            result.hours = int((self.hours + other.hours) + ((self.minutes + other.minutes + (self.seconds + other.seconds - result.seconds) / 60) / 60))
            return result

    def __mul__(self, other):
        if type(other) is not int:
            raise TypeError(lambda x, y: x * y)
        elif other <= 0:
            raise ValueError(lambda x, y: x * y)
        else:
            result = Duration()
            result.seconds = int((self.seconds * other) % 60)
            result.minutes = int(((self.minutes * other) + (self.seconds * other - result.seconds) / 60) % 60)
            result.hours = int((self.hours * other) + ((self.minutes * other) - ((self.minutes * other) % 60)) / 60)
            return result

    def __rmul__(self, other):
        if type(other) is not int:
            raise TypeError(lambda x, y: x * y)
        elif other <= 0:
            raise ValueError(lambda x, y: x * y)
        else:
            result = Duration()
            result.seconds = int((self.seconds * other) % 60)
            result.minutes = int(((self.minutes * other) + (self.seconds * other - result.seconds) / 60) % 60)
            result.hours = int((self.hours * other) + ((self.minutes * other) - ((self.minutes * other) % 60)) / 60)
            return result
