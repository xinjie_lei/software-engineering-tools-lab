#! usr/bin/env python3.4
from timeDuration import Duration


def getTotalDuration(experimentName):
    sum = Duration(hours=0, minutes=0, seconds=0)
    with open("Experiments.txt", 'r') as file:
        lines = file.readlines()[3:]
    for i in lines:
        name, dura, itera = tuple(i.split())
        if name == experimentName:
            minu, secd = dura.split(":")
            if int(itera) > 0:
                duration = Duration(hours=0, minutes=int(minu), seconds=int(secd))
                sum += duration * int(itera)
    return sum


def rankExperiments(*args):
    result = {}
    sort_exp = []
    for i in args:
        duration = str(getTotalDuration(i))
        result[duration] = i
    sort_keys = list(result.keys())
    sort_keys.sort(reverse=True)
    for i in sort_keys:
        sort_exp.append(result[i])
    return sort_exp
