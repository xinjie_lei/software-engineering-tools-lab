#! usr/bin/env python3.4
import sys
import re

from PySide.QtGui import *

from EntryForm import *


class EntryApplication(QMainWindow, Ui_MainWindow):

    states = ["AK", "AL", "AZ", "AR", "CA", "CO", "CT", "DE", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY",
              "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND",
              "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]

    def __init__(self, parent=None):

        super(EntryApplication, self).__init__(parent)
        self.setupUi(self)

        self.allEntries = [self.txtFirstName, self.txtLastName, self.txtAddress, self.txtCity, self.txtState, self.txtZip, self.txtEmail]
        for i in self.allEntries:
            i.textChanged.connect(self.enableSave)

        self.btnSave.setEnabled(False)
        self.btnClear.setEnabled(True)
        self.btnLoad.setEnabled(True)

        self.btnClear.clicked.connect(self.clearForm)
        self.btnSave.clicked.connect(self.saveData)
        self.btnLoad.clicked.connect(self.loadData)

    def enableSave(self):
        self.btnSave.setEnabled(True)
        self.btnLoad.setEnabled(False)

    def clearForm(self):
        for i in self.allEntries:
            i.clear()
        self.lblError.clear()

        self.btnLoad.setEnabled(True)
        self.btnSave.setEnabled(False)

    def saveData(self):
        data = ""
        for i in self.allEntries:
            if i.text() == "":
                self.lblError.setText("Error: {} is not valid!".format(i.objectName()[3:]))
                return
        if self.txtState.text() not in self.states:
            self.lblError.setText("Error: State is not valid!")
            return
        match = re.match(r"[0-9]{5}", self.txtZip.text())
        if match is None:
            self.lblError.setText("Error: Zip is not valid")
            return
        email = re.match(r"\w+@\w+\.\w+", self.txtEmail.text())
        if email is None:
            self.lblError.setText("Error: Email is not valid!")
            return

        data += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
        data += "<user>\n"
        data += "\t<FirstName>" + self.txtFirstName.text() + "</FirstName>\n"
        data += "\t<LastName>" + self.txtLastName.text() + "</LastName>\n"
        data += "\t<Address>" + self.txtAddress.text() + "</Address>\n"
        data += "\t<City>" + self.txtCity.text() + "</City>\n"
        data += "\t<State>" + self.txtState.text() + "</State>\n"
        data += "\t<ZIP>" + self.txtZip.text() + "</ZIP>\n"
        data += "\t<Email>" + self.txtEmail.text() + "</Email>\n"
        data += "</user>\n"

        self.lblError.setText("")
        with open("target.xml", 'w') as file:
            file.write(data)

    def loadFromXmlFile(self, filePath):
        with open(filePath, 'r') as file:
            lines = file.readlines()[2:]
        fname = re.search(r">.*<", lines[0].strip("\t"))
        self.txtFirstName.setText(fname.group(0)[1:-1])
        lname = re.search(r">.*<", lines[1])
        self.txtLastName.setText(lname.group(0)[1:-1])
        addr = re.search(r">.*<", lines[2].strip("\t"))
        self.txtAddress.setText(addr.group(0)[1:-1])
        city = re.search(r">.*<", lines[3].strip("\t"))
        self.txtCity.setText(city.group(0)[1:-1])
        state = re.search(r">.*<", lines[4].strip("\t"))
        self.txtState.setText(state.group(0)[1:-1])
        zip = re.search(r">.*<", lines[5].strip("\t"))
        self.txtZip.setText(zip.group(0)[1:-1])
        email = re.search(r">.*<", lines[6].strip("\t"))
        self.txtEmail.setText(email.group(0)[1:-1])

        self.lblError.setText("")

        self.btnLoad.setEnabled(False)
        self.btnSave.setEnabled(True)

    def loadData(self):
        """
        Obtain a file name from a file dialog, and pass it on to the loading method. This is to facilitate automated
        testing. Invoke this method when clicking on the 'load' button.

        *** DO NOT MODIFY THIS METHOD, OR THE TEST WILL NOT PASS! ***
        """
        filePath, _ = QFileDialog.getOpenFileName(self, caption='Open XML file ...', filter="XML files (*.xml)")

        if not filePath:
            return

        self.loadFromXmlFile(filePath)


if __name__ == "__main__":
    currentApp = QApplication(sys.argv)
    currentForm = EntryApplication()

    currentForm.show()
    currentApp.exec_()
