import numpy as np
from scipy import interpolate
from enum import Enum
import math


class Effect(Enum):
    rotate90 = "rotate90"
    rotate180 = "rotate180"
    rotate270 = "rotate270"
    flipHorizontally = "flipHorizontally"
    flipVertically = "flipVertically"
    transpose = "transpose"


class Homography:

    def __init__(self, **kwargs):
        if "homographyMatrix" not in kwargs and ("sourcePoints" not in kwargs or "targetPoints" not in kwargs):
            raise ValueError("homographyMatrix")
            return
        elif "homographyMatrix" in kwargs:
            if type(kwargs["homographyMatrix"]) is not list:
                raise ValueError("homographyMatrix")
                return
            elif len(kwargs["homographyMatrix"]) != 3:
                raise ValueError("homographyMatrix")
                return
            else:
                for i in kwargs["homographyMatrix"]:
                    for j in i:
                        if type(j) is not float:
                            raise ValueError("homographyMatrix")
                            return
                self.homographyMatrix = np.matrix(kwargs["homographyMatrix"])
        elif "sourcePoints" in kwargs and "targetPoints" in kwargs:
            if len(kwargs["sourcePoints"]) != 4 or len(kwargs["targetPoints"]) != 4:
                raise ValueError("sourcePoints or targetPoints")
            else:
                if "effect" not in kwargs:
                    self.computeHomography(kwargs["sourcePoints"], kwargs["targetPoints"])
                else:
                    self.computeHomography(kwargs["sourcePoints"], kwargs["targetPoints"], effect=kwargs["effect"])

    def computeHomography(self, sourcePoints, targetPoints, effect=None):
        tmp = list(sourcePoints)
        targetPoints = list(targetPoints)
        if isinstance(effect, Effect) is False and effect is not None:
            raise TypeError("effect")
            return
        if effect is Effect.rotate90:
            sourcePoints[0] = tmp[2]
            sourcePoints[1] = tmp[0]
            sourcePoints[2] = tmp[3]
            sourcePoints[3] = tmp[1]
        elif effect is Effect.rotate180:
            sourcePoints[0] = tmp[3]
            sourcePoints[1] = tmp[2]
            sourcePoints[2] = tmp[1]
            sourcePoints[3] = tmp[0]
        elif effect is Effect.rotate270:
            sourcePoints[0] = tmp[1]
            sourcePoints[1] = tmp[3]
            sourcePoints[2] = tmp[0]
            sourcePoints[3] = tmp[2]
        elif effect is Effect.flipHorizontally:
            sourcePoints[0] = tmp[2]
            sourcePoints[1] = tmp[3]
            sourcePoints[2] = tmp[0]
            sourcePoints[3] = tmp[1]
        elif effect is Effect.flipVertically:
            sourcePoints[0] = tmp[1]
            sourcePoints[1] = tmp[0]
            sourcePoints[2] = tmp[3]
            sourcePoints[3] = tmp[2]
        elif effect is Effect.transpose:
            sourcePoints[1] = tmp[2]
            sourcePoints[2] = tmp[1]
        l0 = [sourcePoints[0][0], sourcePoints[0][1], 1, 0, 0, 0, targetPoints[0][0] * -1 * sourcePoints[0][0], targetPoints[0][0] * -1 * sourcePoints[0][1]]
        l1 = [0, 0, 0, sourcePoints[0][0], sourcePoints[0][1], 1, targetPoints[0][1] * -1 * sourcePoints[0][0], targetPoints[0][1] * -1 * sourcePoints[0][1]]
        l2 = [sourcePoints[1][0], sourcePoints[1][1], 1, 0, 0, 0, targetPoints[1][0] * -1 * sourcePoints[1][0], targetPoints[1][0] * -1 * sourcePoints[1][1]]
        l3 = [0, 0, 0, sourcePoints[1][0], sourcePoints[1][1], 1, targetPoints[1][1] * -1 * sourcePoints[1][0], targetPoints[1][1] * -1 * sourcePoints[1][1]]
        l4 = [sourcePoints[2][0], sourcePoints[2][1], 1, 0, 0, 0, targetPoints[2][0] * -1 * sourcePoints[2][0], targetPoints[2][0] * -1 * sourcePoints[2][1]]
        l5 = [0, 0, 0, sourcePoints[2][0], sourcePoints[2][1], 1, targetPoints[2][1] * -1 * sourcePoints[2][0], targetPoints[2][1] * -1 * sourcePoints[2][1]]
        l6 = [sourcePoints[3][0], sourcePoints[3][1], 1, 0, 0, 0, targetPoints[3][0] * -1 * sourcePoints[3][0], targetPoints[3][0] * -1 * sourcePoints[3][1]]
        l7 = [0, 0, 0, sourcePoints[3][0], sourcePoints[3][1], 1, targetPoints[3][1] * -1 * sourcePoints[3][0], targetPoints[3][1] * -1 * sourcePoints[3][1]]
        A = np.matrix([l0, l1, l2, l3, l4, l5, l6, l7])
        b = np.matrix([targetPoints[0][0], targetPoints[0][1], targetPoints[1][0], targetPoints[1][1],
                       targetPoints[2][0], targetPoints[2][1], targetPoints[3][0], targetPoints[3][1]]).transpose()
        ans = np.linalg.solve(A, b)
        self.homographyMatrix = np.matrix([[ans.item(0), ans.item(1), ans.item(2)],
                                           [ans.item(3), ans.item(4), ans.item(5)],
                                           [ans.item(6), ans.item(7), 1.0]])

    def forwardProject(self, point):
        point = point + (1.0,)
        result = self.homographyMatrix * np.matrix(point).transpose()
        result = result / result.item(2)
        result = np.round(result, 2)
        return result.item(0), result.item(1)

    def inverseProject(self, point):
        point = point + (1.0,)
        result = np.linalg.inv(self.homographyMatrix) * np.matrix(point).transpose()
        result = result / result.item(2)
        result = np.round(result, 2)
        return result.item(0), result.item(1)


class Transformation:
    def __init__(self, sourceImage, homography=None):
        if isinstance(sourceImage, np.ndarray) is False:
            raise TypeError("homography")
        if isinstance(homography, Homography) is False and homography is not None:
            raise TypeError("homography")
        else:
            self.image = sourceImage
            self.homography = homography
            self.sourcebox = None
            shape = self.image.shape
            # sourcebox[0] = column = x, sourcebox[1] = row = y
            self.sourcebox = shape[1] - 1, shape[0] - 1

    def setupTransformation(self, targetPoints, effect=None):
        sourcePoints = [(0, 0), (self.sourcebox[0], 0), (0, self.sourcebox[1]), (self.sourcebox[0], self.sourcebox[1])]
        if self.homography is None:
            self.homography = Homography(sourcePoints=sourcePoints, targetPoints=targetPoints, effect=effect)
        self.targetbox = self.computeBoundingHyperCube(targetPoints)

    def computeBoundingHyperCube(self, points):
        li = []
        maxp = []
        minp = []
        for ind in range(2):
            for elmt in points:
                li.append(elmt[ind])
            maxp.append(max(li))
            minp.append(min(li))
            li = []
        return minp, maxp

    def transformImage(self, containerImage):
        if isinstance(containerImage, np.ndarray) is False:
            raise TypeError(containerImage)
        elif np.ndim(containerImage) != 2:
            raise ValueError(containerImage)
        else:
            c_range = range(int(self.targetbox[0][0]), int(self.targetbox[1][0]))
            r_range = range(int(self.targetbox[0][1]), int(self.targetbox[1][1]))
            for c in c_range:
                for r in r_range:
                    col, row = self.homography.inverseProject((c, r))  # col = x, row = y
                    if 0 <= col <= self.sourcebox[0] and 0 <= row <= self.sourcebox[1]:
                        if col.is_integer() and row.is_integer():
                            containerImage[r, c] = self.image[int(row), int(col)]
                        else:
                            if col.is_integer() and row.is_integer() is False:
                                if col == 0:
                                    xgrid = np.array([int(col), int(col + 1)])
                                elif col == self.sourcebox[0]:
                                    xgrid = np.array([int(col - 1), int(col)])
                                else:
                                    xgrid = np.array([int(col) - 1, int(col) + 1])
                                ygrid = np.array([math.floor(row), math.ceil(row)])
                            elif col.is_integer() is False and row.is_integer():
                                if row == 0:
                                    ygrid = np.array([int(row), int(row + 1)])
                                elif row == self.sourcebox[1]:
                                    ygrid = np.array([int(row - 1), int(row)])
                                else:
                                    ygrid = np.array([int(row) - 1, int(row) + 1])
                                xgrid = np.array([math.floor(col), math.ceil(col)])
                            else:
                                xgrid = np.array([math.floor(col), math.ceil(col)]) #xgrid = column, ygrid = row
                                ygrid = np.array([math.floor(row), math.ceil(row)])
                            pixel1 = self.image[ygrid[0], xgrid[0]]
                            pixel2 = self.image[ygrid[0], xgrid[1]]
                            pixel3 = self.image[ygrid[1], xgrid[0]]
                            pixel4 = self.image[ygrid[1], xgrid[1]]
                            pixels = np.array([[pixel1, pixel2], [pixel3, pixel4]])
                            pixel_func = interpolate.RectBivariateSpline(ygrid, xgrid, pixels, kx=1, ky=1)
                            pixel = pixel_func.ev(row, col)
                            containerImage[r, c] = pixel
            return containerImage


class ColorTransformation(Transformation):
    def __init__(self, sourceImage, homography=None):
        if isinstance(sourceImage, np.ndarray) is False:
            raise TypeError("image")
        elif isinstance(homography, Homography) is False and homography is not None:
            raise TypeError("homography")
        elif np.ndim(sourceImage) != 3:
            raise ValueError("soourceimage")
        else:
            self.image = sourceImage
            self.homography = homography
            shape = self.image.shape
            self.sourcebox = shape[1] - 1, shape[0] - 1

    def transformImage(self, containerImage):
        if isinstance(containerImage, np.ndarray) is False:
            raise TypeError(containerImage)
        elif np.ndim(containerImage) != 3:
            raise ValueError(containerImage)
        else:
            c_range = range(int(self.targetbox[0][0]), int(self.targetbox[1][0]) + 1)
            r_range = range(int(self.targetbox[0][1]), int(self.targetbox[1][1]) + 1)
            for r in r_range:
                for c in c_range:
                    col, row = self.homography.inverseProject((c, r))  # col = x, row = y
                    if 0 <= col <= self.sourcebox[0] and 0 <= row <= self.sourcebox[1]:
                        if col.is_integer() and row.is_integer():
                            containerImage[r, c] = self.image[int(row), int(col)]
                        else:
                            if col.is_integer() and row.is_integer() is False:
                                if col == 0:
                                    xgrid = np.array([int(col), int(col) + 1])
                                elif col == self.sourcebox[0]:
                                    xgrid = np.array([int(col) - 1, int(col)])
                                else:
                                    xgrid = np.array([int(col) - 1, int(col) + 1])
                                ygrid = np.array([math.floor(row), math.ceil(row)])
                            elif col.is_integer() is False and row.is_integer():
                                if row == 0:
                                    ygrid = np.array([int(row), int(row) + 1])
                                elif row == self.sourcebox[1]:
                                    ygrid = np.array([int(row) - 1, int(row)])
                                else:
                                    ygrid = np.array([int(row) - 1, int(row) + 1])
                                xgrid = np.array([math.floor(col), math.ceil(col)])
                            else:
                                xgrid = np.array([math.floor(col), math.ceil(col)]) #xgrid = column, ygrid = row
                                ygrid = np.array([math.floor(row), math.ceil(row)])
                            pixel1 = self.image[ygrid[0], xgrid[0]]
                            pixel2 = self.image[ygrid[0], xgrid[1]]
                            pixel3 = self.image[ygrid[1], xgrid[0]]
                            pixel4 = self.image[ygrid[1], xgrid[1]]
                            red = np.array([[pixel1[0], pixel2[0]], [pixel3[0], pixel4[0]]])
                            pixel_func = interpolate.RectBivariateSpline(ygrid, xgrid, red, kx=1, ky=1)
                            red = pixel_func.ev(row, col)
                            green = np.array([[pixel1[1], pixel2[1]], [pixel3[1], pixel4[1]]])
                            pixel_func = interpolate.RectBivariateSpline(ygrid, xgrid, green, kx=1, ky=1)
                            green = pixel_func.ev(row, col)
                            blue = np.array([[pixel1[2], pixel2[2]], [pixel3[2], pixel4[2]]])
                            pixel_func = interpolate.RectBivariateSpline(ygrid, xgrid, blue, kx=1, ky=1)
                            blue = pixel_func.ev(row, col)
                            containerImage[r, c] = [red, green, blue]
            return containerImage