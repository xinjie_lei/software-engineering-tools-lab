#! usr/bin/env python3.4
import sys
import copy
from PySide.QtGui import *
from PySide.QtCore import *
from scipy.misc import *
from HomographyGUI import *
from Homography import *


class HomographyApp(QMainWindow, Ui_MainWindow):

    def __init__(self, parent=None):
        super(HomographyApp, self).__init__(parent)
        self.setupUi(self)

        # ancillary variable
        self.ntpoints = 1
        self.simg = None
        self.timg = None
        self.transimg = None
        self.tpoint = [self.p1, self.p2, self.p3, self.p4]
        self.state = "initial"
        
        self.scanvas.setDisabled(True)
        self.tcanvas.setDisabled(True)
        self.acquire.setDisabled(True)
        self.p1.setDisabled(True)
        self.p2.setDisabled(True)
        self.p3.setDisabled(True)
        self.p4.setDisabled(True)
        self.effect.setDisabled(True)
        self.transform.setDisabled(True)
        self.reset.setDisabled(True)
        self.save.setDisabled(True)

        self.loadsource.clicked.connect(self.ldsimg)
        self.loadtarget.clicked.connect(self.ldtimg)
        self.acquire.clicked.connect(self.acqpoints)
        self.transform.clicked.connect(self.trsfimg)
        self.reset.clicked.connect(self.resetimg)
        self.save.clicked.connect(self.saveimg)
        self.tcanvas.mousePressEvent = self.getpos

    def ldsimg(self):
        sname = QFileDialog.getOpenFileName(self, self.tr("Open Source File"), "~/ee364f05/Lab11/", self.tr("Image Files (*.png)"))
        self.simg = imread(sname[0])
        smap = QPixmap(sname[0])
        sitem = QGraphicsPixmapItem(smap)
        scene = QGraphicsScene()
        scene.addPixmap(smap)
        self.scanvas.setEnabled(True)
        self.scanvas.setScene(scene)
        self.scanvas.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scanvas.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scanvas.fitInView(sitem)
        self.scanvas.show()
        self.scanvas.setDisabled(True)

        if self.state == "transformed":
            timg = QImage(self.timg.data, self.timg.shape[1], self.timg.shape[0], QImage.Format_RGB888)
            self.tcanvas.setEnabled(True)
            tmap = QPixmap.fromImage(timg)
            titem = QGraphicsPixmapItem(tmap)
            scene = QGraphicsScene()
            scene.addPixmap(tmap)
            self.tcanvas.setEnabled(True)
            self.tcanvas.setScene(scene)
            self.tcanvas.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            self.tcanvas.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            self.tcanvas.fitInView(titem)
            self.tcanvas.show()
            for i in self.tpoint:
                i.setText("")
                i.setEnabled(True)
            self.ntpoints = 1
            self.acquire.setEnabled(True)
            self.effect.setDisabled(True)
            self.transform.setDisabled(True)
            self.reset.setDisabled(True)
            self.save.setDisabled(True)
            self.state = "loaded"

    def ldtimg(self):
        tname = QFileDialog.getOpenFileName(self, self.tr("Open Target File"), "~/ee364f05/Lab11/", self.tr("Image Files (*.png)"))
        self.timg = imread(tname[0])
        tmap = QPixmap(tname[0])
        titem = QGraphicsPixmapItem(tmap)
        scene = QGraphicsScene()
        scene.addPixmap(tmap)
        self.tcanvas.setEnabled(True)
        self.tcanvas.setScene(scene)
        self.tcanvas.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.tcanvas.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.tcanvas.fitInView(titem)
        self.tcanvas.show()

        self.acquire.setEnabled(True)

        for i in self.tpoint:
            i.setEnabled(True)
            i.setText("")
        self.ntpoints = 1
        self.tcanvas.setDisabled(True)
        self.effect.setDisabled(True)
        self.transform.setDisabled(True)
        self.reset.setDisabled(True)
        self.save.setDisabled(True)
        self.state = "loaded"

    def acqpoints(self):
        if self.state == "loaded":
            self.state = "select"

        if self.state == "ready":
            self.ntpoints = 1
            for i in self.tpoint:
                i.setText("")
            self.effect.setDisabled(True)
            self.transform.setDisabled(True)
            self.reset.setDisabled(True)
            self.save.setDisabled(True)
            self.state = "select"
        if self.state == "select":
            self.loadsource.setDisabled(True)
            self.loadtarget.setDisabled(True)
            self.tcanvas.setEnabled(True)
            for i in self.tpoint:
                i.setEnabled(True)
            if self.ntpoints < 5:
                for i in self.tpoint:
                    i.setText("")
                self.ntpoints = 1
            else:
                self.loadsource.setEnabled(True)
                self.loadtarget.setEnabled(True)
                self.effect.setEnabled(True)
                self.transform.setEnabled(True)
                self.reset.setEnabled(True)
                self.save.setEnabled(True)
                self.tcanvas.setDisabled(True)
                self.state = "ready"

                
    def getpos(self, event):
        if self.state == "select":
            local_pt = self.tcanvas.mapFromGlobal(event.globalPos())
            img_cord = self.tcanvas.mapToScene(local_pt)
            x = round(img_cord.x(), 2)
            y = round(img_cord.y(), 2)

            if self.ntpoints == 1:
                self.p1.setText(str(x)+", "+str(y))
            if self.ntpoints == 2:
                self.p2.setText(str(x)+", "+str(y))
            if self.ntpoints == 3:
                self.p3.setText(str(x)+", "+str(y))
            if self.ntpoints == 4:
                self.p4.setText(str(x)+", "+str(y))
            if self.ntpoints < 5:
                self.ntpoints += 1
        
    def trsfimg(self):
        tplist = []
        timg = None
        effect = None
        self.transimg = copy.deepcopy(self.timg)
        if self.effect.currentText() == "Rotate 90":
            effect = Effect.rotate90
        elif self.effect.currentText() == "Rotate 180":
            effect = Effect.rotate180
        elif self.effect.currentText() == "Rotate 270":
            effect = Effect.rotate270
        elif self.effect.currentText() == "Flip Horizontally":
            effect = Effect.flipHorizontally
        elif self.effect.currentText() == "Flip Vertically":
            effect = Effect.flipVertically
        elif self.effect.currentText() == "Transpose":
            effect = Effect.transpose
        for i in self.tpoint:
            p = i.text().split(",")
            pn = round(float(p[0]), 2), round(float(p[1]), 2)
            tplist.append(pn)

        if np.ndim(self.simg) == 2:
            transformer = Transformation(self.simg)
            transformer.setupTransformation(targetPoints=tplist, effect=effect)
            self.transimg = transformer.transformImage(self.transimg)
            timg = QImage(self.transimg.data, self.transimg.shape[1], self.transimg.shape[0], QImage.Format_Indexed8)

        elif np.ndim(self.simg) == 3:
            transformer = ColorTransformation(self.simg)
            transformer.setupTransformation(targetPoints=tplist, effect=effect)
            self.transimg = transformer.transformImage(self.transimg)
            timg = QImage(self.transimg.data, self.transimg.shape[1], self.transimg.shape[0], QImage.Format_RGB888)

        self.tcanvas.setEnabled(True)
        tmap = QPixmap.fromImage(timg)
        titem = QGraphicsPixmapItem(tmap)
        scene = QGraphicsScene()
        scene.addPixmap(tmap)
        self.tcanvas.setEnabled(True)
        self.tcanvas.setScene(scene)
        self.tcanvas.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.tcanvas.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.tcanvas.fitInView(titem)
        self.tcanvas.show()

        self.acquire.setDisabled(True)
        for i in self.tpoint:
            i.setDisabled(True)
        self.state = "transformed"

    def saveimg(self):
        name = QFileDialog.getSaveFileName(self, self.tr("Open Save File"), "~/ee364f05/Lab11/", self.tr("Image Files (*.png)"))
        imsave(name[0], self.transimg)

    def resetimg(self):
        if self.state == "transformed":
            if np.ndim(self.timg) == 2:
                timg = QImage(self.timg.data, self.timg.shape[1], self.timg.shape[0], QImage.Format_Indexed8)
            elif np.ndim(self.timg) == 3:
                timg = QImage(self.timg.data, self.timg.shape[1], self.timg.shape[0], QImage.Format_RGB888)
            self.tcanvas.setEnabled(True)
            tmap = QPixmap.fromImage(timg)
            titem = QGraphicsPixmapItem(tmap)
            scene = QGraphicsScene()
            scene.addPixmap(tmap)
            self.tcanvas.setEnabled(True)
            self.tcanvas.setScene(scene)
            self.tcanvas.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            self.tcanvas.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            self.tcanvas.fitInView(titem)
            self.tcanvas.show()
            self.acquire.setEnabled(True)
            for i in self.tpoint:
                i.setEnabled(True)
            self.state = "ready"

if __name__ == "__main__":
    currentApp = QApplication(sys.argv)
    currentForm = HomographyApp()

    currentForm.show()
    currentApp.exec_()