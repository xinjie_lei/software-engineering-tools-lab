import numpy as np
from Homography import ColorTransformation


class AdvancedTransformation:
    def __init__(self, sourceImage, v, h1, h2):
        if isinstance(sourceImage, np.ndarray) is False:
            raise TypeError("homography")
        elif np.ndim(sourceImage) != 3:
            raise ValueError("soourceimage")
        elif sourceImage.shape[1] % 2 != 0:
            raise ValueError(sourceImage.shape[1])
        else:
            self.image = sourceImage
            self.v = v
            self.h1 = h1
            self.h2 = h2

    def applyEffectV(self):
        shape = self.image.shape
        containerImage = np.zeros([(shape[0] + self.v), int(shape[1]), 3], dtype=np.uint8)
        containerImage.fill(255)
        targetPoints = [(0, 0), (shape[1] / 2 - self.h2 - 1, self.v), (self.h1, shape[0] - 1), (shape[1]/2 - 1, shape[0] + self.v - 1)]
        transformer1 = ColorTransformation(self.image[0:shape[0], 0:shape[1]/2])
        transformer1.setupTransformation(targetPoints=targetPoints)
        containerImage = transformer1.transformImage(containerImage)

        targetPoints = [(shape[1]/2 + self.h2, self.v), (shape[1] - 1, 0), (shape[1]/2, shape[0] + self.v - 1), (shape[1] - self.h1 - 1, shape[0] - 1)]
        transformer2 = ColorTransformation(self.image[0:shape[0], shape[1]/2:shape[1]])
        transformer2.setupTransformation(targetPoints=targetPoints)
        containerImage = transformer2.transformImage(containerImage)

        return containerImage

    def applyEffectA(self):
        shape = self.image.shape
        containerImage = np.zeros([(shape[0] + self.v), int(shape[1]), 3], dtype=np.uint8)
        containerImage.fill(255)
        targetPoints = [(self.h1, self.v), (shape[1]/2 - 1, 0), (0, shape[0]+self.v-1), (shape[1]/2 - self.h2 - 1, shape[0] - 1)]
        transformer1 = ColorTransformation(self.image[0:shape[0], 0:shape[1]/2])
        transformer1.setupTransformation(targetPoints=targetPoints)
        containerImage = transformer1.transformImage(containerImage)

        targetPoints = [(shape[1]/2, 0), (shape[1]-self.h1-1, self.v), (shape[1]/2 + self.h2, shape[0] - 1), (shape[1]-1, shape[0] + self.v-1)]
        transformer2 = ColorTransformation(self.image[0:shape[0], shape[1]/2:shape[1]])
        transformer2.setupTransformation(targetPoints=targetPoints)
        containerImage = transformer2.transformImage(containerImage)

        return containerImage