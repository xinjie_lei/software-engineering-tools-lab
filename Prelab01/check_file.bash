#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-08-30 19:46:01 -0400 (Sun, 30 Aug 2015) $
#$Revision: 80492 $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Prelab01/check_file.bash $
#$Id: check_file.bash 80492 2015-08-30 23:46:01Z ee364f05 $

#Check file arrtibutes
if (( $# != 1))
then
    echo "Usage : ./check_file.bash <filename>"
else
    if [[ -e $@ ]] 
    then 
	echo "$@ exists"
    else
	echo "$@ does not exist"
    fi
    if [[ -d $@ ]] 
    then 
	echo "$@ is a directory"
    else 
	echo "$@ is not a directory"
    fi
    if [[ -f $@ ]] 
    then
	echo "$@ is an ordinary file"
    else 
	echo "$@ is not an ordinary file"
    fi
    if [[ -r $@ ]] 
    then 
	echo "$@ is readable"
    else 
	echo "$@ is not readable"
    fi
    if [[ -w $@ ]] 
    then 
	echo "$@ is writable"
    else
	echo "$@ is not writeable"
    fi
    if [[ -x $@ ]] 
    then 
	echo "$@ is executable"
    else
	echo "$@ is not executable"
    fi
fi

exit 0


