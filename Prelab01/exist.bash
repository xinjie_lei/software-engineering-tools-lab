#! /bin/bash

#$Authors$
#$Date: 2015-08-30 15:37:04 -0400 (Sun, 30 Aug 2015) $
#$Revision: 80386 $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Prelab01/exist.bash $
#$Id: exist.bash 80386 2015-08-30 19:37:04Z ee364f05 $

#check if file name exist and readable
for filename in $@
do
    if [[ -r $filename ]]
    then
	echo "File $filename is readable!"
    elif [[ (! -r $filename) && (! -e $filename) ]]
    then
	touch $filename
    fi
done

exit 0
