#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-08-30 16:20:43 -0400 (Sun, 30 Aug 2015) $
#$Revision: 80396 $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Prelab01/line_num.bash $
#$Id: line_num.bash 80396 2015-08-30 20:20:43Z ee364f05 $

#Declare Variables
i=1

#Read from file
if (( $# != 1 ))
then
    echo "Usage: line_num.bash <filename>"
elif [ ! -r $@ ]
then
    echo "Cannot read $@"
else
    exec 4<$@
    while read line <&4
    do  
        echo "$i:$line"
        ((i=$i+1))
    done
fi

exit 0
