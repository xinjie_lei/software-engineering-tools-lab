#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-08-30 19:28:58 -0400 (Sun, 30 Aug 2015) $
#$Revision: 80487 $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Prelab01/sensor_sum.sh $
#$Id: sensor_sum.sh 80487 2015-08-30 23:28:58Z ee364f05 $

sum=0

if (( $# != 1 ))
then
    echo "usage: sensor_sum.sh <filename>"
elif [ ! -r $@ ]
then
    echo "error: $@ is not a readable file"
else
    while read -r line || [[ -n $line ]]
    do
        name=$(echo $line | cut -d '-' -f 1)
        IFS=" " read -a data <<< $line 
        let sum=${data[1]}+${data[2]}+${data[3]}
        echo "$name $sum"
    done <$@
fi

exit 0

