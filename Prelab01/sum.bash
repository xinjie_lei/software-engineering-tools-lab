#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-08-30 15:37:04 -0400 (Sun, 30 Aug 2015) $
#$Revision: 80386 $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Prelab01/sum.bash $
#$Id: sum.bash 80386 2015-08-30 19:37:04Z ee364f05 $

#No need to use shift 
#Declare Variables
sum=0

#Sum all the command-line arguments
for arg in $@
do
    ((sum=$sum+$arg))
done

echo "$sum"

exit 0
