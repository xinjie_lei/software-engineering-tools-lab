#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-08-30 18:21:21 -0400 (Sun, 30 Aug 2015) $
#$Revision: 80467 $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Prelab01/svncheck.bash $
#$Id: svncheck.bash 80467 2015-08-30 22:21:21Z ee364f05 $

if (($# != 0))
then
    echo "Usage: ./svncheck.bash <nofile>"
else
    exec 4<file_list        
    while read -r line || [[ -n $line ]]
    do
        if [[ ! -e $line ]]
        then 
            echo "Error: File $line appears to not exist here or in svn"
        else
            #Get status and executable attribute of file
            STATUS=$(svn status $line | head -c 1)
            EXECUT=$(ls -l $line | head -c 4)
            
            #Not in SVN and Not Executable
            if [[ STATUS == "?" ]] && [[ EXECUT != "x" ]] 
            then
                read -p "Would you like to make $line executable?" ans
                if [[ ans=="y" ]] 
                then 
                    chmod +x $line
                fi
            svn add $line
            fi

            #In SVN and Not Executable
            [[ STATUS == "" ]] && [[ EXECUT != "x" ]] && svn propset svn:executable ON $line
        fi
    done <&4

    svn commit -m "Auto-commiting code"
fi

exit 0
