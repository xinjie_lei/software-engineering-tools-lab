#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-09-05 01:42:50 -0400 (Sat, 05 Sep 2015) $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Prelab02/process_temps.bash $
#$Revision: 81382 $
#$Id: process_temps.bash 81382 2015-09-05 05:42:50Z ee364f05 $

if (($# != 1))
then
    echo "Usage: process_temps.bash <input file>"
    exit 1
elif [[ ! -r $@ ]]
then
    echo "Error: $@ is not readable"
    exit 2
else
     while read -r line || [[ -n $line ]]
     do
         temp=($line)
         time_t=${temp[0]}
         unset temp[0]
         temp=( "${temp[@]}" )
         for item in ${temp[*]}
         do
             ((aver=$aver+$item))
         done
         ((aver=$aver/${#temp[*]}))
         echo "Average temperature for time $time_t was $aver C."
         aver=0
     done < <(tail -n +2 $@)

fi

exit 0

