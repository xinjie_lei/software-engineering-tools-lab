#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-09-06 22:51:20 -0400 (Sun, 06 Sep 2015) $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Prelab02/run.bash $
#$Revision: 81464 $
#$Id: run.bash 81464 2015-09-07 02:51:20Z ee364f05 $

ftt=1000000
caches=(1 2 4 8 16 32)
width=(1 2 4 8 16)

if (($# != 2))
then
    echo "Usage: ./run.bash <filename1> <filename2>"
    exit 1
elif [[ ! -r $1 ]] 
then
    echo "$@ is not readable"
    exit 2
else
    if ! gcc $1 -o quick_sim
    then
        echo "error: $1 could not be compiled!"
        exit 3
    else 
        if [[ ! -e $2 ]]
        then
             filename=$2
             touch $filename
        else
            read -p "$2 exists. Would you like to delete it? " ans
            if [[ $ans = "y" ]] || [[ $ans = "yes" ]]
            then
                rm $2
                filename=$2
                touch $filename
            else
                read -p "Enter a new filename: " filename
                touch $filename
            fi
        fi
        for i in ${caches[*]}
         do
             for j in ${width[*]}
             do
                 amd=$(quick_sim $i $j a) 
                 prca=$(echo $amd | cut -d':' -f2)
                 cpia=$(echo $amd | cut -d':' -f8 | cut -d':' -f2)
                 exta=$(echo $amd | cut -d':' -f10 | cut -d':' -f2)
                 echo "$prca:$i:$j:$cpia:$exta" >>$filename
                 intel=$(quick_sim $i $j i)
                 prci=$(echo $intel | cut -d':' -f2)
                 cpii=$(echo $intel | cut -d':' -f8 | cut -d':' -f2)
                 exti=$(echo $intel | cut -d':' -f10 | cut -d':' -f2)
                 echo "$prci:$i:$j:$cpii:$exti" >>$filename
                 if (($exta > $exti))
                 then
                     if (($ftt>$exti))
                     then
                        ftt=$exti
                        ftprc=$prci
                        ftcache=$i
                        ftwidth=$j
                    fi
                else
                    if (($ftt>$exta))
                    then
                        ftt=$exta
                        ftprc=$prca
                        ftcache=$i
                        ftwidth=$j
                    fi
                fi
            done
        done
        echo "Fastest run time achived by $ftprc with cache size $ftcache and issue width $ftwidth was $ftt"
            
    fi
fi

exit 0
