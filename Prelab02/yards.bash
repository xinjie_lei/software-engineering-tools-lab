#! /bin/bash

#$Author: ee364f05 $
#$Date: 2015-09-05 01:42:50 -0400 (Sat, 05 Sep 2015) $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Prelab02/yards.bash $
#$Revision: 81382 $
#$Id: yards.bash 81382 2015-09-05 05:42:50Z ee364f05 $

max=0

if (($# != 1))
then
    echo "Usage: yards.bash <filename>"
    exit 1
elif [[ ! -r $@ ]]
then
    echo "Error: $@ is not readable"
    exit 2
else
    while read -r line || [[ -n $line ]]
    do
        yards=($line)
        name=$yards
        unset yards[0]
        yards=( "${yards[@]}" )
        for item in ${yards[*]}
        do
            ((aver=$aver+$item))
        done
        ((total=${#yards[*]}))
        ((aver=$aver/$total))
        for item in ${yards[*]}
        do
            ((sub=$item-$aver))
            ((vari=$vari+$sub**2))
        done
        ((vari=$vari/$total))
        echo "$name schools averaged $aver yards receiving with a variance of $vari"
        (( $max < $aver )) && max=$aver
        aver=0
        total=0
        vari=0
    done < $@
    echo "The largest average yardage was $max"
fi

exit 0
