#! /usr/bin/env python3.4

import glob
import filecmp
from pprint import pprint as pp

#$Authors$
#$Date: 2015-09-21 14:57:26 -0400 (Mon, 21 Sep 2015) $
#$Revision: 82013 $
#$HeadURL: svn+ssh://ece364sv@ecegrid-thin1/home/ecegrid/a/ece364sv/svn/F15/students/ee364f05/Prelab04/dataStructs.py $
#$Id: dataStructs.py 82013 2015-09-21 18:57:26Z ee364f05 $

def getWordFrequency():
    dict = {}
    filelist = glob.glob('./files/*.txt')
    for i in filelist:
        with open(i, 'r') as file:
            for line in file:
                for item in line.split(" "):
                    if item not in dict:
                        dict[item] = 1
                    else:
                        dict[item] += 1
    return dict


def getDuplicates():
    dict = {}
    filelist = glob.glob('./files/*.txt')
    wc = 0
    while len(filelist) > 1:
        group = []
        others = []
        master = filelist[0]
        group.append(master[8:11])
        for other in filelist[1:]:
            if filecmp.cmp(master, other):
                group.append(other[8:11])
            else:
                others.append(other)
        with open(master, 'r') as myFile:
            for line in myFile:
                line = line.replace(",", "")
                line = line.replace(".", "")
                wc += len(set(line.split(" ")))
        dup = (wc, group)
        dict[master[8:11]] = dup
        filelist = others
        wc = 0
    return dict


def getPurchaseReport():
    dict = {}
    price = {}
    filelist = glob.glob('./purchases/*.txt')
    total = 0
    with open("./purchases/Item List.txt", 'r') as listitem:
            list = listitem.readlines()
            list = list[2:]
            for i in list:
                pair = i.split()
                price[pair[0]] = float(pair[1].replace("$", ""))
    for j in filelist[1:]:
        with open(j, 'r') as file:
            transac = file.readlines()
            transac = transac[2:]
            for k in transac:
                item = k.split()
                total += price[item[0]] * int(item[1])
        dict[int(j[23])] = round(total, 2)
        total = 0
    return dict


def getTotalSold():
    dict = {}
    filelist = glob.glob('./purchases/*.txt')
    for i in filelist[1:]:
        with open(i, 'r') as file:
            line = file.readlines()
            line = line[2:]
            for j in line:
                item = j.split()
                if item[0] in dict:
                    dict[item[0]] += int(item[1])
                else:
                    dict[item[0]] = int(item[1])
    return dict