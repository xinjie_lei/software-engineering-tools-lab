#! usr/bin/env python3.4
import re
import sys


def refine(filename):
    with open(filename, 'r') as file:
        line = file.readlines()
    for i in line:
        strList = i.split()
        id = re.search(r"\w+@", strList[0])
        print(id.group() + "ecn.purdue.edu" + "    " + strList[1] + r"\100")
    return 0


def main(argv):
    refine(argv)
    return 0


if __name__ == '__main__':
    main(sys.argv[1])
