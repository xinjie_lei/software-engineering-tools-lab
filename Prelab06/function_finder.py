#! usr/bin/env python3.4
import sys
import re
import os


def find(argv):
    if os.path.exists(argv) and os.access(argv, os.R_OK):
        with open(argv, 'r') as file:
            line = file.readlines()
        for i in line:
            match = re.match(r"^def\s+[a-zA-Z](\w|_|-)*\s*\((((\w|_|-|=)*,)\s*)*(\w|_|-|=)*\s*\)\s*:$", i)
            if match is not None:
                strlist = match.group().replace(":", "").replace(")", "").split("(")
                print(strlist[0].split()[1])
                arglist = strlist[1].split(",")
                n = 1
                for i in arglist:
                    i = i.strip()
                    print("Arg" + str(n) + ": " + i)
                    n += 1
    else:
        print("Error: Could not read " + argv)
    return 0


def main(argv):
    find(argv)
    return 0

if __name__ == '__main__':
    if len(sys.argv) == 2:
        main(sys.argv[1])
    else:
        print("Usage: function_finder.py [python_file_name]")

