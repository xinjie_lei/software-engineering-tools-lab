#! usr/bin/env python3.4
import re
import sys


def detect(filename):
    with open(filename, 'r') as file:
        line = file.readlines()
    for i in line:
        strList = i.split(":")
        strList[1] = strList[1].replace("\n", "")
        validityIP = re.match(r"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", strList[0])
        validityPort = re.match(r"^\d+$", strList[1])
        if validityPort is not None:
            if (validityIP is not None) and (int(strList[1]) >= 1) and (int(strList[1]) <= 1024):
                print(strList[0] + ":" + strList[1] + " - Valid (root privileges required)")
            if (validityIP is not None) and (32767 >= int(strList[1])) and (int(strList[1])> 1024):
                print(strList[0] + ":" + strList[1] + " - Valid")
            if (validityIP is not None) and (int(strList[1]) < 1 or int(strList[1]) > 32767):
                print(strList[0] + ":" + strList[1] + " - Invalid Port number")
            if validityIP is None:
                print(strList[0] + ":" + strList[1] + " - Invalid IP Address")
        else:
            print(strList[0] + ":" + strList[1] + " - Invalid Port Number")
    return 0


def main(argv):
    detect(argv)
    return 0

if __name__ == '__main__':
    main(sys.argv[1])
