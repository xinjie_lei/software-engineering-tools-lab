#! usr/bin/env python3.4

import math

class PointND:

    def __init__(self, *args):
        if type(args) is tuple:
            for i in args:
                if type(i) is not float:
                    raise ValueError("Cannot instantiate an object with non-float values.")
            self.t = args
            self.n = len(args)
        else:
            raise ValueError("Cannot instantiate an object with non-float values.")

    def __str__(self):
        string = '('
        for i in self.t:
            string = string + format(round(i, 2), '.2f') + ", "
        return string[:-2] + ")"

    def __hash__(self):
        return hash(self.t)

    def distanceFrom(self, other):
        dist = 0
        if other.n != self.n:
            raise ValueError("Cannot calculate distance between points of different cardinality.")
        else:
            for i, j in zip(self.t, other.t):
                dist += (i-j)**2
            return math.sqrt(dist)

    def nearestPoint(self, points):
        mindist = 10000
        if len(points) == 0:
            raise ValueError("Input cannot be empty.")
        else:
            for i in points:
                dist = self.distanceFrom(i)
                if mindist > dist:
                    mindist = dist
                    point = i
            return point

    def clone(self):
        clone = PointND(*self.t)
        return clone

    def __add__(self, other):
        sum = []
        if isinstance(other, PointND):
            if len(other.t) != len(self.t):
                raise ValueError("Cannot operate on points with different cardinalities.")
            else:
                for i, j in zip(self.t, other.t):
                    sum.append(i + j)
                return PointND(*tuple(sum))
        elif isinstance(other, float):
            for i in self.t:
                sum.append(i + other)
            return PointND(*tuple(sum))

    def __radd__(self, other):
        sum = []
        for i in self.t:
                sum.append(i + other)
        return PointND(*tuple(sum))

    def __sub__(self, other):
        dif = []
        if isinstance(other, PointND):
            if len(other.t) != len(self.t):
                raise ValueError("Cannot operate on points with different cardinalities.")
            else:
                for i, j in zip(self.t, other.t):
                    dif.append(i - j)
                return PointND(*tuple(dif))
        elif isinstance(other, float):
            for i in self.t:
                dif.append(i - other)
            return PointND(*tuple(dif))

    def __mul__(self, other):
        dif = []
        if isinstance(other, PointND):
            if len(other.t) != len(self.t):
                raise ValueError("Cannot operate on points with different cardinalities.")
            else:
                for i, j in zip(self.t, other.t):
                    dif.append(i * j)
                return PointND(*tuple(dif))
        elif isinstance(other, float):
            for i in self.t:
                dif.append(i * other)
            return PointND(*tuple(dif))

    def __rmul__(self, other):
        dif = []
        for i in self.t:
                dif.append(i * other)
        return PointND(*tuple(dif))

    def __truediv__(self, other):
        dif = []
        if isinstance(other, PointND):
            if len(other.t) != len(self.t):
                raise ValueError("Cannot operate on points with different cardinalities.")
            else:
                for i, j in zip(self.t, other.t):
                    dif.append(i / j)
                return PointND(*tuple(dif))
        elif isinstance(other, float):
            for i in self.t:
                dif.append(i / other)
            return PointND(*tuple(dif))

    def __neg__(self):
        neg = []
        for i in self.t:
                neg.append(-i)
        return PointND(*tuple(neg))

    def __getitem__(self, item):
        return self.t[item]

    def __eq__(self, other):
        if other.n != self.n:
            raise ValueError("Cannot compare points with different cardinalities.")
        else:
            for i, j in zip(self.t, other.t):
                if i != j:
                    return False
            return True

    def __ne__(self, other):
        if self == other:
            return False
        else:
            return True

    def __gt__(self, other):
        if other.n != self.n:
            raise ValueError("Cannot compare points with different cardinalities.")
        else:
            origin = PointND(*tuple([0.0] * self.n))
            if self.distanceFrom(origin) > other.distanceFrom(origin):
                return True
            else:
                return False

    def __ge__(self, other):
        if self > other or self == other:
            return True
        else:
            return False

    def __lt__(self, other):
        if self >= other:
            return False
        else:
            return True

    def __le__(self, other):
        if self < other or self == other:
            return True
        else:
            return False


class Point3D(PointND):

    def __init__(self, *args):
        if type(args) is tuple:
            for i in args:
                if type(i) is not float:
                    raise ValueError("Cannot instantiate an object with non-float values.")
            PointND.__init__(self, *args)
            self.x = args[0]
            self.y = args[1]
            self.z = args[2]
        else:
            raise ValueError("Cannot instantiate an object with non-float values.")


class PointSet:

    def __init__(self, **kwargs):
        if not kwargs:
            self.points = set()
            self.n = 0
        elif "pointList" not in kwargs:
            raise KeyError("'pointList' input parameter not found.")
        elif "pointList" in kwargs:
            if not kwargs["pointList"]:
                raise ValueError("'pointList' input parameter cannot be empty.")
            else:
                self.points = set(kwargs["pointList"])
                self.n = kwargs["pointList"][0].n
                for i in self.points:
                    if i.n != self.n:
                        raise ValueError("Expecting a point with cardinality {0}.".format(self.n))

    def addPoint(self, p):
        if p.n != self.n:
            raise ValueError("Expecting a point with cardinality {0}.".format(self.n))
        else:
            li = list(self.points)
            li.append(p)
            self.points = set(li)

    def count(self):
        return len(self.points)

    def computeBoundingHyperCube(self):
        li = []
        maxp = []
        minp = []
        for ind in range(self.n):
            for elmt in self.points:
                li.append(elmt[ind])
            maxp.append(max(li))
            minp.append(min(li))
            li = []
        maxpoint = PointND(*tuple(maxp))
        minpoint = PointND(*tuple(minp))
        return minpoint, maxpoint

    def computeNearestNeighbors(self, otherPointSet):
        li = []
        neib = PointND()
        for i in self.points:
            distance = 10000
            for j in otherPointSet.points:
                dist = i.distanceFrom(j)
                if distance > dist:
                    distance = dist
                    neib = j
            tup = (i, neib)
            li.append(tup)
        li.sort()
        return li

    def __add__(self, other):
        if other.n != self.n:
            raise ValueError("Expecting a point with cardinality {0}.".format(self.n))
        else:
            li = list(self.points)
            li.append(other)
            self.points = set(li)
            return self

    def __sub__(self, other):
        if other.n != self.n:
            raise ValueError("Expecting a point with cardinality {0}.".format(self.n))
        else:
            if other in self.points:
                self.points.remove(other)
            return self

    def __contains__(self, item):
        if item in self.points:
            return True
        else:
            return False