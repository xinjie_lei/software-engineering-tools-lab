#! usr/bin/env python3.4

values = input("Please enter some values: ").split(" ")

sum = 0
for x in values:
    try:
        sum += float(x)
    except ValueError as v:
        pass

print(sum)
