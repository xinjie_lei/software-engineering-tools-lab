#! usr/bin/env python3.4


def find_median(List_1, List_2):
    li = List_1.copy()
    li.extend(List_2)
    li.sort()
    if len(li) % 2 == 0:
        idx = int((len(li) - 1) / 2)
    else:
        idx = int(len(li) / 2)
    return li[idx], li

