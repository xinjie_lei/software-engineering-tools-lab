#! usr/bin/env python3.4

import listmod

li1 = [int(x) for x in input("Enter the first list of numbers: ").split(" ")]
li2 = [int(x) for x in input("Enter the second list of numbers: ").split(" ")]

print("First list: {}".format(li1))
print("Second list: {}".format(li2))

(Median, Sorted_List) = listmod.find_median(li1, li2)

if Sorted_List is None:
    print("Merged list: Not implemented")
else:
    print("Merged list: {}".format(Sorted_List))

if Median is None:
    print("Median: Not Implemented")
else:
    print("Median: {}".format(Median))


