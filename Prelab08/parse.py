#! usr/bin/env python3.4

import sys

try:
    fp = open(sys.argv[1], 'r')
except IndexError:
    sys.stderr.write("Usage: parse.py [filename]")
    sys.exit(1)
except IOError:
    sys.stderr.write("{} is not a readable file".format(sys.argv[1]))
    sys.exit(2)

sum = 0
len = 0
notes = []
all_notes = []
for line in fp:
    strarr = line.split(" ")
    for i in strarr:
        try:
            sum += float(i)
            len += 1
        except ValueError as v:
            notes.append(i.replace("\n", ""))
    if len == 0:
        all_notes.append(" ".join(notes))
    else:
        all_notes.append("{:0.3f}".format(sum/len) + " " + " ".join(notes))
    notes = []
    sum = 0
    len = 0
print("\n".join(all_notes))
