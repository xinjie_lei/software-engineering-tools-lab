import sys
from PySide.QtCore import *
from PySide.QtGui import *
from calculator import *


class simpleCalc(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        self.result = 0
        self.lastop = ""
        self.flagop = 0
        self.flagsep = 0
        self.flagdot = 0
        self.flagin = 0
        self.dg = 0

        super(simpleCalc, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle("Calculator")
        self.display.setAlignment(QtCore.Qt.AlignRight)
        self.display.setText("0")

        self.nums = [self.pb0, self.pb1, self.pb2, self.pb3, self.pb4, self.pb5, self.pb6, self.pb7, self.pb8, self.pb9]
        self.ops = [self.pbadd, self.pbsub, self.pbmul, self.pbdiv]

        for i in self.nums:
            i.clicked.connect(self.updateText)

        for i in self.ops:
            i.clicked.connect(self.op)

        self.pbclr.clicked.connect(self.clearText)
        self.pbdot.clicked.connect(self.updateTextDot)
        self.pbequal.clicked.connect(self.equal)
        self.sep.stateChanged.connect(self.setSepFlag)
        self.decidg.activated.connect(self.setdg)

    def setdg(self):
        self.dg = int(self.decidg.currentText())

    def setSepFlag(self):
        if self.sep.isChecked():
            self.flagsep = 1
            if self.display.text() != "":
                num = float(self.display.text().replace(",", ""))
                if num.is_integer():
                    self.display.setText('{0:,}'.format(int(num)))
                else:
                    self.display.setText('{0:,}'.format(num))
        else:
            self.flagsep = 0
            if self.display.text() != "":
                num = float(self.display.text().replace(",", ""))
                if num.is_integer():
                    self.display.setText(str(int(num)))
                else:
                    self.display.setText(str(num))



    def clearText(self):
        self.display.setText("")
        self.result = 0
        self.lastop = ""
        self.flagop = 0

    def updateText(self):
        i = self.sender()
        text = self.display.text().replace(",", "")
        if (len(text) <= 11 and "." not in text) or (len(text) <= 12 and "." in text) or self.flagin:
            if text != "":
                if text == "0":
                    self.display.setText(i.text())
                else:
                    if self.flagop == 0:
                        if self.flagsep == 1:
                            if self.flagdot == 0:
                                deci = float(text)*10 + float(i.text())
                                if deci.is_integer():
                                    self.display.setText('{:,}'.format(int(deci)))
                                else:
                                    self.display.setText('{0:,}'.format(deci))
                            else:
                                if (text[::-1].find('.')) < self.dg:
                                    self.display.insert(i.text())
                        else:
                            if (text[::-1].find('.')) < self.dg:
                                self.display.insert(i.text())
                    else:
                        if self.flagdot == 0:
                            self.display.setText(i.text())
                        else:
                            if (text[::-1].find('.')) < self.dg:
                                self.display.insert(i.text())
            else:
                self.display.setText(i.text())
        self.flagop = 0
        self.flagin = 0

    def op(self):
        if self.lastop == "":
            self.result = round(float(self.display.text().replace(",", "")), self.dg)
        elif self.lastop == "=":
            pass
        else:
            self.calcInterresult()
        if len(str(self.result)) > 14:
            self.display.setText("Result Too Big.")
            self.flagin = 0
        else:
            if self.flagsep:
                if self.result.is_integer():
                    self.display.setText('{0:,}'.format(int(self.result)))
                else:
                    self.display.setText('{0:,}'.format(self.result))
            else:
                if self.result.is_integer():
                    self.display.setText(str(int(self.result)))
                else:
                    self.display.setText(str(self.result))
            self.flagop = 1
            self.flagdot = 0
            self.flagin = 1

        i = self.sender()
        if i.text() == "+":
            self.lastop = "+"
        if i.text() == "-":
            self.lastop = "-"
        if i.text() == "x":
            self.lastop = "*"
        if i.text() == "/":
            self.lastop = "/"

    def equal(self):
        self.calcInterresult()
        if len(str(self.result)) > 14:
            self.display.setText("Result Too Big.")
        else:
            if self.flagsep:
                self.display.setText('{0:,}'.format(self.result))
            else:
                if self.result.is_integer():
                    self.display.setText(str(int(self.result)))
                else:
                    self.display.setText(str(self.result))
        self.lastop = "="
        self.flagop = 1

    def updateTextDot(self):
        if self.dg > 0:
            if self.flagop == 0:
                self.display.insert(".")
            else:
                self.display.setText("0.")
            self.flagdot = 1


    def calcInterresult(self):
        if self.display.text() != "Result Too Big.":
            if self.lastop == "+":
                self.result += float(self.display.text().replace(",", ""))
            if self.lastop == "-":
                self.result -= float(self.display.text().replace(",", ""))
            if self.lastop == "*":
                self.result *= float(self.display.text().replace(",", ""))
            if self.lastop == "/":
                self.result /= float(self.display.text().replace(",", ""))
            self.result = round(self.result, self.dg)


if __name__ == "__main__":
    calcApp = QApplication(sys.argv)
    calcForm = simpleCalc()

    calcForm.show()
    calcApp.exec_()